---
layout: layout.liquid
title: Archives
---
Archives
=======

<ul>
{%- for post in collections.ressource -%}
<li><a href="{{ post.url }}">{{ post.data.title }}</a></li>
{%- endfor -%}
</ul>
