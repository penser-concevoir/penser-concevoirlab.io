---
layout: layout.liquid
title: Un site 11ty
---
Un site 11ty
=======
Commencez par la simplicité déconcertante de <a href="https://www.11ty.dev/docs/getting-started/">Getting start</a> et comme vous pourriez très vite rencontrer un problème avec certains formats de fichiers (comme par exemple .css), créez un fichier ```.eleventy.js``` à la racine de votre dossier 11ty et copiez-collez-y ce qui suit :
```
module.exports = {
    templateFormats: [
      "html",
      "md",
      "css",
      "gif",
      "png",
      "jpg"
    ],
    passthroughFileCopy: true
  };
```
