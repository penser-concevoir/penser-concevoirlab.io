module.exports = {
  templateFormats: [
    "html",
    "md",
    "css",
    "gif",
    "png",
    "jpg"
  ],
  passthroughFileCopy: true
};
