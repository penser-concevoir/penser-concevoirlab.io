---
layout: layout.liquid
title: Le contexte contemporain
tags:
  - contexte
  - ressource

---
Le contexte contemporain
=======

+ [Où atterrir ?](https://www.editionsladecouverte.fr/ou_atterrir_-9782707197009) ou comment s'orienter en politique avec Bruno Latour
+ [Où suis-je ?](https://www.editionsladecouverte.fr/ou_suis_je_-9782359252019) de Bruno Latour
+ Les [objectifs de développement durable](https://www.un.org/sustainabledevelopment/fr/) des Nations Unis
+ Le podcast [Dites à l'avenir que nous arrivons](https://play.acast.com/s/dites-a-lavenir-que-nous-arrivons) - Dans chaque épisode Mathieu Baudin, le Directeur de l’Institut des Futurs souhaitables, reçoit une exploratrice ou un explorateur de mondes qui nous partage son regard sur l’époque, nous confie ses visions de futurs souhaitables et ses contributions concrètes pour les faire advenir.

Merci de ne pas acheter vos livres sur AZ mais dans des [Librairies indépendantes](https://www.librairiesindependantes.com/)
