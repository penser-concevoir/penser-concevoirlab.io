---
layout: layout.liquid
title: Ressources, guides et outils pour une sobriété numérique
tags:
  - sobriete
  - ressource
  - outils
  - conception
---
Ressources, guides et outils pour une sobriété numérique
=======
+ Retrouvez sur le site de Gauthier Roussilhe, [un guide de conversion numérique au low tech](http://gauthierroussilhe.com/fr/posts/convert-low-tech) très complet.
+ Vous pouvez aussi jeter un coup d'œil aux [audits d'écoconception](https://ldevernay.github.io/Audits.html) de Laurent Devernay.
+ La [check-list écoconception web](https://collectif.greenit.fr/ecoconception-web/115-bonnes-pratiques-eco-conception_web.html) vise à faciliter la mise en oeuvre des bonnes pratiques. Gratuite et ouverte (licence Creative Commons), elle est disponible au format HTML, PDF, et Excel. Cette checklist est complétée par une méthode d'évaluation de la maturité (ci-dessous) et par un référentiel de conformité (guide d'évalution) qui permet à tout le monde d'évaluer la mise en œuvre des bonnes pratiques de la même façon.
+ Abonnez-vous à la newsletter [Curiously Green](https://www.wholegraindigital.com/curiously-green/) de l'agence Wholegrain Digital.
+ [Le guide d’éco-conception de services numériques](https://eco-conception.designersethiques.org/guide/) publié par l'association des Designers Éthiques.

Outils
-------
Attention, vous allez voir que les résultats diffèrent de sites en sites. Cherchez avant tout des ordres de grandeur.
+ [EcoIndex](http://www.ecoindex.fr/) (Comment est calculé le poids CO2?)
+ [Website Carbon Calculator](https://www.websitecarbon.com/) (Comment est calculé le poids CO2?)

Voir aussi
-------
+ [supercooldesign.co.uk/blog/sustainable-coding-and-hosting-tips](https://supercooldesign.co.uk/blog/sustainable-coding-and-hosting-tips)
