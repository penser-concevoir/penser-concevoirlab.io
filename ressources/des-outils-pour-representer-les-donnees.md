---
layout: layout.liquid
title: Des outils pour représenter les données
tags:
  - data
  - ressource

---
Des outils pour représenter les données
=======
+  Explorer, visualiser et analyser les données. Collaborer avec la communauté. Apprendre et être inspiré. Partagez vos connaissances avec le monde entier sur [Observable](https://observablehq.com/)

## Ressources

+ Découvrir le [Graphic presentation](https://archive.org/details/graphicpresentat00brinrich/mode/2up) de Willard Cope Brinton
+ [datavizcatalogue.com](https://datavizcatalogue.com/index.html) - Un catalogue des types de dataviz

## Et des données

+ [datagir]https://datagir.ademe.fr/ de l'Ademe
+ 
