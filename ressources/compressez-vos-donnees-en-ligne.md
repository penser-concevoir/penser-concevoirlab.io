---
layout: layout.liquid
title: Compressez vos données en ligne
tags:
  - sobriete
  - ressource
  - outils
---
Compressez vos données en ligne
=======
+ Guide de Gauthier Roussilhe sur le site du Shift Project pour apprendre [Comment réduire en 5 minutes le poids d’une vidéo tout en gardant une bonne qualité?](https://theshiftproject.org/guide-reduire-poids-video-5-minutes/)

## Compression d'images

+ [squoosh.app/](https://squoosh.app/)
