---
layout: layout.liquid
title: Polices de caractères
tags:
  - typographie
  - ressource
---

Polices de caractères
=======
+ [Use & Modify](https://usemodify.com/) - Une sélection de typographies Open Source par Raphaël Bastide.
+ [Fonts in Use](https://fontsinuse.com/) - An independent archive of typography.
+ [Fontstand](https://fontstand.com/) is a font discovery platform that allows you to test and use high-quality fonts on all platforms.
+ [Pousse ta fonte](https://www.poussetafonte.com/) - un index des fonderies.
+ [Badass Libre Fonts by Womxn](https://www.design-research.be/by-womxn/)
+ [typotheque.le75.be/](http://typotheque.le75.be/) - bibliothèque numérique ‘open source’ (téléchargement gratuit) dans laquelle sont enregistrées toutes les typographies créées par les étudiants de l’option graphisme. Chaque année de nouvelles créations vont voir le jour et vont alimenter cette collection. C’est un outil très utile pour les étudiants de l’option et leurs projets mais également pour tout ceux intéressés par la typographie.
+ [https://typotheque.luuse.fun/](https://typotheque.luuse.fun/)
+ Pour retrouver la police d'un logo [fontinlogo.com/](https://www.fontinlogo.com/).

## Fonderies

+ [sharptype.co/](https://sharptype.co/)
+ [typotheque.genderfluid.space](https://typotheque.genderfluid.space/) - La typothèque Bye Bye Binary présente des fontes inclusif·ves, non-binaires, post-binaires en construction.
+ [Velvetyne.fr/](http://velvetyne.fr/) - We are VTF, or the Velvetyne Type Foundry. Since 2010, we’ve designing and distributing free and open source typefaces. We also organise ass-kicking workshops for humans and computers.

## Et aussi:

+ [Liste des caractères Unicode](https://en.wikipedia.org/wiki/List_of_Unicode_characters).
