---
layout: layout.liquid
title: Des siècles d'impressions sur LetterFormArchive
tags:
  - inspiration
  - typographie
  - ressource

---
Des siècles d'impressions sur LetterFormArchive
=======
+ Commencez par consulter la collection complète du magazine [Emigre](http://oa.letterformarchive.org/search?dims=Firms&vals0=Emigre&friendly0=Emigre) en commençant par les grands formats comme le [numéro 19](http://oa.letterformarchive.org/item?workID=lfa_emigre_0019) comme le [numéro 28](http://oa.letterformarchive.org/item?workID=lfa_emigre_0028) et son jeu entre la grille apparente et l'audace maîtrisée de la mise en page.
+ Retrouvez [toutes les archives](http://oa.letterformarchive.org/search?searchType=1&searchText=*)
