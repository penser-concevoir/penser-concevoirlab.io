---
layout: layout.liquid
title: Alternatives à Google Search
tags:
  - ressource
  - outils
  - anonymat
---
Alternatives à Google Search
=======

Suite à une discussion sur le Slack des Designers Éthiques à propos des alternatives à Google Search, je pense que remplacer Google Search par DuckDuckGo est pas loin d'être la même chose. C'est ce passage obligé pour accéder à l'information qui est troublant, bien au-delà de toute récupération de données. Ce réflexe, développé en une vingtaine d'années, me fait parfois l'impression de prendre la voiture et l’autoroute pour aller chercher mon pain au coin de la rue. Je fais donc ici une liste de moteurs de recherche ciblés - donc utiles.

## Définitions

+ [CNRTL](https://www.cnrtl.fr/definition/)

## Cartes et itinéraires

+ [OpenStreetMap](https://www.openstreetmap.org/)

## Revues de sciences humaines et sociales

+ [OpenEdition](https://openedition.org/) — portail de ressources électroniques en sciences humaines et sociales.
+ [Isidore](https://isidore.science/) - Votre assistant de recherche en Sciences Humaines et Sociales

## Typographie

+ [Fonts in Use](https://fontsinuse.com/) - An independent archive of typography.
+ [MyFonts](https://www.myfonts.com/) - MyFonts offers the largest selection of professional fonts for any project.
+ [WhatTheFont](https://www.myfonts.com/WhatTheFont/) - Instant font identification powered by the world’s largest collection of fonts.
+ [Font Squirrel](https://www.fontsquirrel.com/) - Free fonts have met their match. We know how hard it is to find quality freeware that is licensed for commercial work. We've done the hard work!

## À trier

+ ~~thestocks.im/~~

-------

&rarr; Voir [Fuire le Cloud et les GAFAM](https://fuir-le-cloud.gitlab.io/)
