---
layout: layout.liquid
title: Du Web Design brutaliste
tags:
  - performance
  - sobriete
  - fonctionnel
  - ressource

---
Du Web Design brutaliste
=======
L'approche fonctionnelle de Josef Müller-Brockmann a inspiré quelques Web Designers qui ont mis ses conseils en application:
+ [Brutalist Web Design](https://brutalist-web.design/) de David Bryant Copeland
+ [Susty](https://sustywp.com/), le thème Wordpress poids plume de Jack Lenox
