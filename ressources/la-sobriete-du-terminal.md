---
layout: layout.liquid
title: La sobriété du terminal
tags:
  - outils
  - accessible
  - ressource

---
La sobriété du terminal
=======

Le terminal pour faire l'expérience d'une sobriété numérique.
+ De la retouche et de la manipulation d'images sans interface graphique avec [ImageMagick](https://imagemagick.org/). La documentation est [ici](https://imagemagick.org/Usage/)
+
