---
layout: layout.liquid
title: Une réécriture du style suisse
tags:
  - typographie
  - inspiration
  - ressource

---
Une réécriture du style suisse
=======

+ [Wolfgang Weingart](http://indexgrafik.fr/wolfgang-weingart/)
+ [8vo](http://indexgrafik.fr/studio-8vo/) - Agence de graphic design londonienne fondée in 1985 par Simon Johnston, Mark Holt et Hamish Muir.
+ [Spassky Fischer](http://spassky-fischer.fr/fr/) - Bureau de design graphique concret et photographie sensible.
