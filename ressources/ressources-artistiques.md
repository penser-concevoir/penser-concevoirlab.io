---
layout: layout.liquid
title: Ressources artistiques
tags:
  - inspiration
  - ressource

---
Ressources artistiques
=======

+  Pour mieux comprendre les enjeux de l'art contemporain, vous pouvez commencer par lire quelques dossiers pédagogiques du musée Pompidou sur les artistes et mouvements suivants : [Marcel Duchamp](http://mediation.centrepompidou.fr/education/ressources/ENS-Duchamp/ENS-duchamp.htm), [Art Conceptuel](http://mediation.centrepompidou.fr/education/ressources/ENS-ArtConcept/ENS-ArtConcept.htm), [Pop Art](http://mediation.centrepompidou.fr/education/ressources/ENS-Duchamp/ENS-duchamp.htm)...
+ [UbuWeb](https://www.ubuweb.com/) — Founded in 1996, UbuWeb is a pirate shadow library consisting of hundreds of thousands of freely downloadable avant-garde artifacts.
+ [Rhizome](https://rhizome.org/) - The ArtBase is Rhizome's archive of digital art, freely accessible to the public online.
+ [Monoskop](https://monoskop.org/), a wiki for arts, media and humanities.
+ [ Net Art Anthology](https://anthology.rhizome.org/)
+ At the intersection of contemporary art, research and hacking, [disnovation.org](https://disnovation.org/) develop situations of disruption, speculation, and debate, in order to question dominant techno-positivist ideologies, and to stimulate post-growth narratives. they edited the pirate book, an anthology on media piracy.
+ [Atlas of Places](https://www.atlasofplaces.com/) is a public educational collection of Academia, Architecture, Cartography, Cinema, Essays, Painting, Photography, Research.
