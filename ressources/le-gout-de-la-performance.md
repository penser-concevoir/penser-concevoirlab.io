---
layout: layout.liquid
title: Le goût de la performance
tags:
  - performance
  - ressource

---
Le goût de la performance
=======
C'est une  chance que les sites à l'empreinte carbone élevée aient très souvent de mauvaises performances business. Cela peut-être un levier pour un client qui n'aurait que faire de sauver la planète.

Tester la performance d'un site avec :
+ [Pingdom](https://tools.pingdom.com/)
+ Google vous propose [Test my site](https://www.thinkwithgoogle.com/feature/testmysite/) ou pour aller plus loin l'extension [Lighthouse](https://chrome.google.com/webstore/detail/lighthouse/blipmdconlkpinefehnmjammfjpmpbjk) pour le navigateur Chrome (cette fonctionnalité est aussi directement accessible dans ce navigateur : _control-clic > Inspecter_)
