---
layout: layout.liquid
title: Une rigueur typographique
tags:
  - typographie
  - ressource

---
Une rigueur typographique
=======

La lisibilité d'un document tient à peu de choses.
+ [Petites leçons de typographie](https://jacques-andre.fr/faqtypo/lessons.pdf) de Jacques André
+ [Practical Typography](https://practicaltypography.com/) de Matthew Butterick
