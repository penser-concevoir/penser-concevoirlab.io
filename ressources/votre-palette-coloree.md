---
layout: layout.liquid
title: Votre palette colorée
tags:
  - outils
  - couleurs
  - ressource

---
Votre palette colorée
=======

Quelques outils :

+ Le [Color Tool](https://material.io/resources/color/) de Google
+ [The color of anything](https://picular.co/)
+ (à compléter)

Et pour ne pas oublier qu'embrasser les contraintes facilite le travail de conception : [Des couleurs accessibles](/ressources/des-couleurs-accessibles/)


## Le nom des couleurs

Pour le plaisir poétique d'utiliser dans un fichier CSS des noms de couleurs au lieu de codes hexadécimaux [HTML Color Names](https://htmlcolorcodes.com/color-names/)
