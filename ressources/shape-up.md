---
layout: layout.liquid
title: Shape Up
tags:
  - conception
  - projet
  - ressource

---
Shape Up
=======
Pour aller au delà de l'Agile ou du Kanban, lire le livre [ShapeUp](https://basecamp.com/shapeup/webbook) de Ryan Singer (Basecamp) en anglais mais très bien illustré.
