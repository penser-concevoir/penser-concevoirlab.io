---
layout: layout.liquid
title: Des couleurs accessibles
tags:
  - accessibilite
  - couleurs
  - ressource

---
Des couleurs accessibles
=======

+ [Constrast Checker](https://webaim.org/resources/contrastchecker/)
+ [Tanaguru Constrast Finder](https://contrast-finder.tanaguru.com/)
+ [Accessible Color Matrix](https://toolness.github.io/accessible-color-matrix/)
+ (à compléter)

Pour d'autres liens, lire l'excellent article de Stéphanie Walter [accessibilité et couleurs, outils et ressources pour concevoir des produits accessibles](https://stephaniewalter.design/fr/blog/accessibilite-et-couleurs-outils-et-ressources-pour-concevoir-des-produits-accessible/)

Voir aussi : [Votre palette colorée](/ressources/votre-palette-coloree/)
