---
layout: layout.liquid
title: Les créateurs d'une esthétique numérique minimaliste
tags:
  - inspiration
  - typographie
  - sobriete
  - ressource

---
Les créateurs d'une esthétique numérique minimaliste
=======
+ Un des atouts de la réussite du Macintosh s’appelle [Susan Kare](http://kare.com/). Elle rejoint Apple en 1982 pour créer une série d’icônes pour le système, ainsi que des polices de caractères. Lire l'article [Index Graphik](http://indexgrafik.fr/susan-kare/).
