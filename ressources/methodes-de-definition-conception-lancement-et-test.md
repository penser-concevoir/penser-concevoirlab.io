---
layout: layout.liquid
title: Méthodes de définition, conception, lancement et test (Design Thinking)
tags:
  - methodes
  - outils
  - conception
---

Méthodes de définition, conception, lancement et test (Design Thinking)
=======

+ Pour un design tourné vers l'humain, l'agence Ideo vous propose d'utiliser son [DesignKit](https://www.designkit.org/).
+ La Design School de Stanford a sélectionné des [ressources](https://dschool.stanford.edu/resources) qu'elle vous invite à _hacker_. Commencer par le [Design Thinking Bootleg](https://dschool.stanford.edu/resources/design-thinking-bootleg).
+ La fondation Nesta a conçu un DIY Toolkit pour stimuler l'émergence d'idées, de concepts et de produits. Le PDF en français est ici: [https://media.nesta.org.uk/documents/DIY-French_ybdo8lH.pdf](https://media.nesta.org.uk/documents/DIY-French_ybdo8lH.pdf)

## ressources

+ [50 methods Kit](https://fromsmash.com/methodes_UX_8)
