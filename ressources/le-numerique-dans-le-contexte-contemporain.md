---
layout: layout.liquid
title: Le numérique dans le contexte contemporain
tags:
  - contexte
  - sobriete
  - ressource

---
Le numérique dans le contexte contemporain
=======
La conception de systèmes numériques est trop souvent le résultat d'une pensée hors-sol, voici quelques références pour revenir sur terre :
+ [Situer le numérique](https://designcommun.fr/cahiers/situer-le-numerique/) de Gauthier Roussilhe
+ [Que peut le numérique pour la transition écologique?](https://gauthierroussilhe.com/pdf/NTE-Mars2021.pdf) de Gauthier Roussilhe (encore lui!)
+ [Déployer-la-sobriete-numerique](https://theshiftproject.org/article/deployer-la-sobriete-numerique-rapport-shift/), le nouveau rapport du Shift sur l’impact environnemental du numérique
+ Un guide de l'Ademe [La face cachée du numérique](https://www.ademe.fr/sites/default/files/assets/documents/guide-pratique-face-cachee-numerique.pdf) - Réduire les impacts du numérique sur l'environnement

## livres pour comprendre les enjeux du numérique

+ [Design et humanités numériques](https://editions-b42.com/produit/design-et-humanites-numeriques/) — Anthony Masure. Contenu complémentaire sur [esthetique-des-donnees.editions-b42.com](http://esthetique-des-donnees.editions-b42.com/#index).

## Interviews sur le site Designers éthiques

+ [Anthony Masure](https://livre-ethique-numerique.designersethiques.org/content/interviews/interview-anthony_masure.html)
+ [Gauthier Roussilhe](https://livre-ethique-numerique.designersethiques.org/content/interviews/interview-gauthier_roussilhe.html)

## Sur les low-tech

+ [Low tech : face au tout-numérique, se réapproprier les technologies](https://www.coredem.info/rubrique85.html) - Ce nouveau numéro de la collection Passerelle s’inscrit dans le cadre des réflexions, sociales et politiques, de plus en plus nombreuses autour de la question des low tech.
