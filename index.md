---
layout: layout.liquid
title: Accueil
---
Ressources pour penser et concevoir (ou refuser de concevoir) des solutions numériques
=======

<mark>En écriture</mark>

Ce site est pensé et conçu sous le signe de la sobriété. L'ultime sobriété eut été de ne pas le concevoir mais je cherche à documenter ici avec le minimum d'empreinte CO<sub>2</sub>, quelques ressources qui pourraient être utiles aux étudiants avec lesquels je travaille. Il ne s'agit pas d'être un index de solutions clé en main, qui n'attendraient qu'à être copiées et collées, mais un ensemble de thèmes, idées et liens qui permettent de construire sa propre démarche créative.
— pyc — Septembre 2021 —

<section class="annonce">

<span class="titre">À écouter</span> Le podcast [Dites à l'avenir que nous arrivons](https://play.acast.com/s/dites-a-lavenir-que-nous-arrivons) - Dans chaque épisode Mathieu Baudin, le Directeur de l’Institut des Futurs souhaitables, reçoit une exploratrice ou un explorateur de mondes qui nous partage son regard sur l’époque, nous confie ses visions de futurs souhaitables et ses contributions concrètes pour les faire advenir.

</section>

Contexte
-------
<ul>
{%- for post in collections.contexte -%}
<li><a href="{{ post.url }}">{{ post.data.title }}</a></li>
{%- endfor -%}
</ul>

Inspiration
-------
<ul>
{%- for post in collections.inspiration -%}
<li><a href="{{ post.url }}">{{ post.data.title }}</a></li>
{%- endfor -%}
</ul>

Conception
--------
<ul>
{%- for post in collections.conception -%}
<li><a href="{{ post.url }}">{{ post.data.title }}</a></li>
{%- endfor -%}
</ul>

Sobriété
-------
<ul>
{%- for post in collections.sobriete -%}
<li><a href="{{ post.url }}">{{ post.data.title }}</a></li>
{%- endfor -%}
</ul>

Accessibilité
-------
<ul>
{%- for post in collections.accessibilite -%}
<li><a href="{{ post.url }}">{{ post.data.title }}</a></li>
{%- endfor -%}
</ul>

Typographie
-------
<ul>
{%- for post in collections.typographie -%}
<li><a href="{{ post.url }}">{{ post.data.title }}</a></li>
{%- endfor -%}
</ul>

Couleurs
--------
<ul>
{%- for post in collections.couleurs -%}
<li><a href="{{ post.url }}">{{ post.data.title }}</a></li>
{%- endfor -%}
</ul>

Performance (???)
-------
<ul>
{%- for post in collections.performance -%}
<li><a href="{{ post.url }}">{{ post.data.title }}</a></li>
{%- endfor -%}
</ul>

Visualisation de données
-------
<ul>
{%- for post in collections.data -%}
<li><a href="{{ post.url }}">{{ post.data.title }}</a></li>
{%- endfor -%}
</ul>

Gestion de projet
-------
<ul>
{%- for post in collections.projet -%}
<li><a href="{{ post.url }}">{{ post.data.title }}</a></li>
{%- endfor -%}
</ul>

Outils
-------
<ul>
{%- for post in collections.outils -%}
<li><a href="{{ post.url }}">{{ post.data.title }}</a></li>
{%- endfor -%}
</ul>
